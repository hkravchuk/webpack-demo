import _ from 'lodash'; // for global variable _
import './style.css';
import Icon from './rework_logo.png';
import Data from './data.xml';
import Notes from './data.csv';
import yaml from './data.yaml';
import printMe from './print.js';

console.log(yaml.title); // output `YAML Example`
console.log(yaml.owner.name); // output `Tom Preston-Werner`

function component() {
    const element = document.createElement('div');
    const btn = document.createElement('button');

    element.innerHTML = _.join(['Hello', 'webpack'], ' ');
    element.classList.add('hello');

    // Add the image to our existing div.
    const myIcon = new Image();
    myIcon.src = Icon;

    element.appendChild(myIcon);

    btn.innerHTML = 'Click me and check the console!';
    btn.onclick = printMe;

    element.appendChild(btn);

    console.log(Data);
    console.log(Notes);

    return element;
}

document.body.appendChild(component());