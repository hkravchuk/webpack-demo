# webpack-demo

Run `npm run build` to build project.

`npm run build -- --color` - to add more options

`webpack --config webpack.config.js` - If a webpack.config.js is present, the webpack command picks it up 
by default, but you can pass a configuration of any name

### Links

- [Webpack Getting Started](https://webpack.js.org/guides/getting-started/)